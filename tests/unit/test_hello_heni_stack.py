import aws_cdk as core
import aws_cdk.assertions as assertions

from hello_heni.hello_heni_stack import HelloHeniStack


def test_lambda_created():
    app = core.App()
    stack = HelloHeniStack(app, "hello-heni")
    template = assertions.Template.from_stack(stack)

    template.has_resource_properties("AWS::Lambda::Function", 
        {
            "Handler": "hello_heni.handler",
            "Runtime": "python3.7",
        }
    )

def test_api_gateway_created():
    app = core.App()
    stack = HelloHeniStack(app, "hello-heni")
    template = assertions.Template.from_stack(stack)

    template.has_resource_properties("AWS::ApiGateway::RestApi", 
        {
            "Description": "Displays a hello message.",
            "Name": "Hello Heni"
        }
    )
