import json
import os

def handler(event, context):
    """
    Say hello to Heni.
    """

    print('request: {}'.format(json.dumps(event)))
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'text/plain'
        },
        'body': 'Hello Heni! From {}\n'.format(os.environ.get('AUTHOR'))
    }