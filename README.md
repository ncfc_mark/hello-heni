
# Hello Heni app

An app to say hello to Heni.

## Deploy

The AWS CDK requires the installation of the aws cli and npm on your local environment.

Create a python virtual environment to install the dependancies.

```
$ python3 -m venv .venv
```

Activate the virtual environment by running the following command.

```
$ source .venv/bin/activate
```

You now need to install the dependencies with the following command.

```
$ pip install -r requirements.txt
```

Before deploying, you will need an AWS account and a user with AWS key credentials.
If you haven't got this already, run the following command to configure your local environment.

```
$ aws configure
```

Run cdk synth to generate the CloudFormation templates for the application.

```
$ cdk synth
```

To deploy the stack to your AWS account, run

```
$ cdk deploy
```

## Tests

To run the tests, install the dependacies in the dev requirements file

```
$ pip install -r requirements-dev.txt
```

Use the following command to run the tests

```
python3 -m pytest
```

### TODO:

Add more request template status code responses.

Add more tests!

Improve documentation.
