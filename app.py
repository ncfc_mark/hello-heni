#!/usr/bin/env python3
import os

import aws_cdk as cdk

from hello_heni.hello_heni_stack import HelloHeniStack


app = cdk.App()
HelloHeniStack(app, "HelloHeniStack")
app.synth()
