from aws_cdk import (
    Stack,
    aws_apigateway as _apigateway,
    aws_lambda as _lambda,
    aws_iam as _iam
)
from constructs import Construct

class HelloHeniStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # IAM role for executing the lambda function
        lambda_role = _iam.Role(scope=self, id='hello-heni-lambda-role',
            assumed_by =_iam.ServicePrincipal('lambda.amazonaws.com'),
            role_name='hello-heni-lambda-role',
            managed_policies=[
                _iam.ManagedPolicy.from_aws_managed_policy_name(
                    'service-role/AWSLambdaVPCAccessExecutionRole'
                ),
                _iam.ManagedPolicy.from_aws_managed_policy_name(
                   'service-role/AWSLambdaBasicExecutionRole'
                )
            ])

        # Lambda resource
        hello_heni_lambda = _lambda.Function(
            self, 'hello-heni-lambda',
            runtime=_lambda.Runtime.PYTHON_3_7,
            function_name='hello-heni-lambda',
            description='Lambda function deployed using AWS CDK Python',
            code=_lambda.Code.from_asset('./lambda'),
            handler='hello_heni.handler',
            role=lambda_role,
            environment={
                'NAME': 'hello-heni-lambda',
                'AUTHOR': 'Mark Smithson'
            }
        )

        # API Gateway - for public accesibility
        heni_api = _apigateway.RestApi(self, "hello-heni-api",
            rest_api_name="Hello Heni",
            description="Displays a hello message.")

        hello_heni_lambda_integration = _apigateway.LambdaIntegration(hello_heni_lambda,
            request_templates={"application/json": '{ "statusCode": "200" }'})

        heni_api.root.add_method("GET", hello_heni_lambda_integration)